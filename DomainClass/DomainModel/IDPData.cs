﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClass.DomainModel
{
    public class IDPData
    {
        public int ID { get; set; }
        public string SHMeli { get; set; }
        public string Addr { get; set; }
        public string CodePosti { get; set; }
        public string Mobile { get; set; }
        public string Pic { get; set; }
        public string DateSabt { get; set; }
        public int? UserID { get; set; }
        public virtual User User { get; set; }

        public IDPData()
        {

        }
    }
}
