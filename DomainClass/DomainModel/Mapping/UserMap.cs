﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClass.DomainModel.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            this.HasKey(t => t.ID);
            this.ToTable(nameof(User));
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.LFName).HasColumnName("LFName");
            this.Property(t => t.UserName).HasColumnName("UserNAme");
            this.Property(t => t.Password).HasColumnName("Password");

        }
    }
}
