﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataLayer.DataAccessLayer;
using DomainClass.DomainModel;
namespace IDP
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //JetEntityFrameworkProvider.JetConnection.ShowSqlStatements = true;
            using (var dbc = new DataBaseContext())
            {
                var user = dbc.User.FirstOrDefault(t => t.UserName == textBox1.Text & t.Password == textBox2.Text);
                if (user != null)
                {
                    this.Close();
                    FrmMain frmmain = new FrmMain(user.LFName, user.ID);
                    frmmain.Show();
                }
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
