﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DomainClass.DomainModel;
using DomainClass.DomainModel.Mapping;
namespace DataLayer.DataAccessLayer
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext() : base("DefaultConnection")
        {
            Configuration.LazyLoadingEnabled = true;
        }
        static DataBaseContext()
        {
            Database.SetInitializer(new DataBaseContextInitializer());
        }
        public DbSet<Office> Office { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<IDPData> IDPData { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new OfficeMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new IDPDataMap());
        }
    }
}
