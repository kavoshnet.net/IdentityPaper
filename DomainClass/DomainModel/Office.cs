﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClass.DomainModel
{
    public class Office
    {
        [Key]
        public int ID { get; set; }
        public string OfficeName { get; set; }
        public string OfficeNumber { get; set; }
        public string OfficeManager { get; set; }
        public Office()
        {

        }

    }
}
