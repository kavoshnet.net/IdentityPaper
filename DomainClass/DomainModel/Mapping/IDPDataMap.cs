﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClass.DomainModel.Mapping
{
    public class IDPDataMap : EntityTypeConfiguration<IDPData>
    {
        public IDPDataMap()
        {
            this.HasKey(t => t.ID);
            this.ToTable(nameof(IDPData));
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.SHMeli).HasColumnName("SHMeli");
            this.Property(t => t.Addr).HasColumnName("Addr");
            this.Property(t => t.CodePosti).HasColumnName("CodePosti");
            this.Property(t => t.Mobile).HasColumnName("Mobile");
            this.Property(t => t.Pic).HasColumnName("Pic");
            this.Property(t => t.DateSabt).HasColumnName("DateSabt");
            this.Property(t => t.UserID).HasColumnName("UserID");
            //Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.IDPData)
                .HasForeignKey(d => d.UserID);
        }
    }
}
