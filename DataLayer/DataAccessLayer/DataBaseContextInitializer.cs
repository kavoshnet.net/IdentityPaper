﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace DataLayer.DataAccessLayer
{
    //class DataBaseContextInitializer : DropCreateDatabaseAlways<DataBaseContext>
    //class DataBaseContextInitializer : CreateDatabaseIfNotExists<DataBaseContext>
    class DataBaseContextInitializer : DropCreateDatabaseIfModelChanges<DataBaseContext>
    //class DataBaseContextInitializer : MigrateDatabaseToLatestVersion<DataAccessLayer.DataBaseContext, Migrations.Configuration>
    {
        public DataBaseContextInitializer()
        {

        }
        protected override void Seed(DataBaseContext context)
        {
            base.Seed(context);
            context.User.Add(
                new DomainClass.DomainModel.User
                { LFName = "کاربر اصلی", UserName = "Admin", Password = "1234" }
                );
        }
    }
}
