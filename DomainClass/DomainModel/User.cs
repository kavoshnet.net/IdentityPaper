﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClass.DomainModel
{
    public class User
    {
        public int ID { get; set; }
        public string LFName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public virtual ICollection<IDPData> IDPData { get; set; }
        public User()
        {
            IDPData = new List<IDPData>();
        }

    }
}
