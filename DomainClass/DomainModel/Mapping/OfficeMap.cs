﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainClass.DomainModel.Mapping
{
    public class OfficeMap: EntityTypeConfiguration<Office>
    {
        public OfficeMap()
        {
            this.HasKey(t => t.ID);
            this.ToTable(nameof(Office));
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.OfficeName).HasColumnName("OfficeNAme");
            this.Property(t => t.OfficeNumber).HasColumnName("OfficeNumber");
            this.Property(t => t.OfficeManager).HasColumnName("OfficeManager");

        }
    }
}
