﻿using DataLayer.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyClasses;
namespace IDP
{
    public partial class FrmMain : Form
    {
        public string LFName;
        public int UserID;
        public FrmMain()
        {
            InitializeComponent();
        }
        public FrmMain(string _LFName, int _UserID)
        {
            LFName = _LFName;
            UserID = _UserID;
            InitializeComponent();
        }

        private void FrmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            using (var dbc = new DataBaseContext())
            {
                var Now = MyClasses.PersianDate.Now();
                var idpdata = dbc.IDPData.FirstOrDefault(t => t.SHMeli == textBox1.Text & t.DateSabt == Now);
                if (idpdata != null)
                {
                    idpdata.SHMeli = textBox1.Text;
                    idpdata.Addr = textBox2.Text;
                    idpdata.CodePosti = textBox3.Text;
                    idpdata.Mobile = textBox4.Text;
                    idpdata.UserID = UserID;
                    dbc.SaveChanges();
                    MessageBox.Show($"اطلاعات شما با موفقیت به روز رسانی  شد");
                    ShowData();

                }
                else
                {
                    dbc.IDPData.Add(
                        new DomainClass.DomainModel.IDPData
                        {
                            SHMeli = textBox1.Text,
                            Addr = textBox2.Text,
                            CodePosti = textBox3.Text,
                            Mobile = textBox4.Text,
                            DateSabt = Now,
                            UserID = UserID
                        }
                        );
                    dbc.SaveChanges();
                    MessageBox.Show($"اطلاعات شما با موفقیت ذخیره  شد");
                    ShowData();

                }
                ClearText();
            }

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void ShowData()
        {
            using (var dbc = new DataBaseContext())
            {
                var query = dbc.IDPData.Select(t => new { t.SHMeli, t.Addr, t.CodePosti, t.Mobile, t.DateSabt })
                    .Where(t => t.DateSabt.CompareTo(textBox5.Text) >= 0 && t.DateSabt.CompareTo(textBox6.Text) <= 0);
                var count = query.Count();
                label8.Text = "تعداد رکورد: " + count + "مورد";
                var result = dbc.IDPData.Select(t => new { t.SHMeli, t.Addr, t.CodePosti, t.Mobile, t.DateSabt })
                    .Where(t => t.DateSabt.CompareTo(textBox5.Text) >= 0 && t.DateSabt.CompareTo(textBox6.Text) <= 0).ToList();
                dataGridView1.DataSource = result;
                dataGridView1.Columns["SHMeli"].HeaderText = "شماره ملی";
                dataGridView1.Columns["Addr"].HeaderText = "آدرس";
                dataGridView1.Columns["CodePosti"].HeaderText = "کدپستی";
                dataGridView1.Columns["Mobile"].HeaderText = "موبایل";
                dataGridView1.Columns["DateSabt"].HeaderText = "تاریخ ثبت";
            }
            ShowPicture();
        }
        private void FrmMain_Shown(object sender, EventArgs e)
        {
            textBox5.Text = MyClasses.PersianDate.Now();
            textBox6.Text = MyClasses.PersianDate.Now();
            ShowData();
        }
        private void ClearText()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox1.Focus();
            ShowPicture();
        }
        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = dataGridView1.CurrentRow.Index;
            textBox1.Text = dataGridView1.CurrentRow.Cells["SHMeli"].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells["Addr"].Value.ToString();
            textBox3.Text = dataGridView1.CurrentRow.Cells["CodePosti"].Value.ToString();
            textBox4.Text = dataGridView1.CurrentRow.Cells["Mobile"].Value.ToString();
            ShowPicture();
        }

        private void ShowPicture()
        {
            if (System.IO.File.Exists(@"C:\temp\images\" + textBox1.Text + ".jpg"))
                pictureBox1.Image = new Bitmap(@"C:\temp\images\" + textBox1.Text + ".jpg");
            else
                pictureBox1.Image = null;

        }
        private void Button3_Click(object sender, EventArgs e)
        {
            ClearText();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            using (var dbc = new DataBaseContext())
            {
                int rowindex = dataGridView1.CurrentRow.Index;
                var Now = dataGridView1.CurrentRow.Cells["DateSabt"].Value.ToString();
                var idpdata = dbc.IDPData.FirstOrDefault(t => t.SHMeli == textBox1.Text & t.DateSabt == Now);
                if (idpdata != null)
                {
                    dbc.IDPData.Remove(idpdata);
                    dbc.SaveChanges();
                    MessageBox.Show($"اطلاعات شما با موفقیت حذف  شد");
                    ShowData();
                }
            }

        }

        private void Button5_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var filename = openFileDialog1.FileName;
                //var newfilename = filename.Replace(System.IO.Path.GetFileName(filename), textBox1.Text)+".jpg";
                if (System.IO.File.Exists(filename) == true)
                {
                    System.IO.File.Copy(filename, @"C:\temp\images\" + textBox1.Text + ".jpg");
                    pictureBox1.Image = new Bitmap(@"C:\temp\images\" + textBox1.Text + ".jpg");
                }
            }
        }
    }
}
